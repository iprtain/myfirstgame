import pygame
import core.keys as keyss
import core.collision_check as cc
import core.game_conditions as gc

#sets the name of the app in the top border of window
pygame.display.set_caption('Game_stand_off')

#main loop
while keyss.keys.run:

	#collision check
	cc.collision.enemybullet_collision()
	cc.collision.playerbullet_collision()

	#controls and switch for game stages
	keyss.keys.keys()

	#defines what game stage is being drawn
	gc.game_conditions.playing()
