import classes.instances.player_and_npc as pn
import core.keys as keyss
import core.collision_check as cc
import core.game_window as gw
import pygame
import imageload.imageload as img
import core.time_functions as t


class GameConditions():
	def __init__(self):
		#sets the window resolution
		self.win = pygame.display.set_mode((1500,900))

	def intro(self):
		self.win.blit(img.intropic,(0,0))
		pygame.display.update()

	def gamewonact(self):
		self.win.blit(img.gamewonpic,(120,100))
		pygame.display.update()

	def gamelostact(self):
		self.win.blit(img.gamelostpic,(600,300))
		pygame.display.update()


	def gamelost(self):
		keyss.keys.gamelost = True

	#this function tracks what stage of the game is being displayed
	def playing(self):
			if not pn.characters.player.alive:
				t.timer.marktime()
				t.timer.delay(2, self.gamelost)
				#print('gamelost')
			if keyss.keys.introd == True:
				pygame.mouse.set_visible(True)
				self.intro()

			elif keyss.keys.gamewon:
				self.gamewonact()

			elif keyss.keys.gamelost:
				#print('gamelost2')
				self.gamelostact()


			else:
				pygame.mouse.set_visible(False)   #disables regular cursor
				gw.gamewindow.drawgamewindow()


			num = len(cc.collision.enemies)
			count = 0


			for enemy in cc.collision.enemies:
				if not enemy.alive:
					count += 1

			if count == num:
				keyss.keys.gamewon = True
			count = 0

game_conditions = GameConditions()