import time

#this is class that serves as stopwatch 
#i made it in the end so it it is mostly not used
class Timer():
	def __init__(self):
		self.start = None
		self.future = None

		self.done = True
		self.marked = True

		self.timestamp = 0
		self.rof = 0

	def marktime(self):
		if self.marked:
			self.start = time.time()

			self.marked = False

	def delay(self, sec, function):
		self.future = self.start + sec
		if time.time() >= self.future and self.done:
			print('start = ' + str(self.start))
			print('SADA ' + str(time.time()))
			print('FUTURE ' + str(self.future))
			function()
			self.done = False
			self.marked = True

	def add_timestamp(self):                   
		self.timestamp = time.time()

	def add_rof(self):                             
		self.rof = time.time()

	def measure(self,sec):                          
		elapsed = time.time() - self.timestamp
		if elapsed >= sec:
			return True	

	def measure_rof(self,sec):                           
		elapsed = time.time() - self.rof
		if elapsed >= sec:
			return True

	



timer = Timer()