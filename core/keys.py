import pygame
import time
import classes.instances.player_and_npc as pn
import core.click as klik
from core.time_functions import *
import classes.bullet_class as cbullet
import core.reset as re


class Keys():
	def __init__(self):
		#player reloaded status
	#it disables shooting key
		self.reloaded = True
		self.clock = pygame.time.Clock()

		#game stages
		self.introd = True
		self.gamewon = False
		self.gamelost = False

		self.run = True


	#it repeats for mouse click and space, fires bullet - creates instance of player bullet class
	def fire(self):
		
		if klik.player_click.click == 5: # remove this and put it in click.py
			klik.player_click.reloaded = False
		if klik.player_click.reloaded == True and timer.measure(1) and timer.measure_rof(0.2):
			#print('this iiiiiis timestamp ' + str(timestamp))
			#player shooting animation activated
			pn.characters.player.shoot = True
			#count +1 times that space has been pressed (counts fired bullets)
			klik.player_click.add()
			# add instance of bullet class  into bullets list
			cbullet.bullets.append(cbullet.Bullet(pn.characters.player.x, pn.characters.player.y, pn.characters.player.width, pn.characters.player.height))
					
			# activate bullet
			#print(player_click.click)
			cbullet.bullets[klik.player_click.click].fired = True
			timer.add_rof()

	def keys(self):

	#player controls
		self.clock.tick(60)

		
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				self.run = False

			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_RETURN:
					if self.introd == True:
						self.introd = False
					elif self.gamewon == True:
						self.introd = True
						self.gamewon = False
						re.reset()

					elif self.gamelost == True:
						self.introd = True
						self.gamelost = False
						re.reset()
						if pn.characters.player.alive:
							print('itsaliiiive')

			if event.type == pygame.MOUSEMOTION:
				mouse_position = pygame.mouse.get_pos()
				#print(mouse_position)
				#print(ai3.enemy_bullets[len(ai3.enemy_bullets) - 1].fired)

			if event.type == pygame.MOUSEBUTTONDOWN:
				self.fire()


		#this line enables registering if a key is being held down for a prolonged amaunt of time instead of just being tapped once
		keys = pygame.key.get_pressed()	

		if pn.characters.player.alive:

			if keys[pygame.K_a] and pn.characters.player.x > pn.characters.player.vel:
				pn.characters.player.x -= pn.characters.player.vel
				pn.characters.player.orientation = 1
				pn.characters.player.left = True
				pn.characters.player.right = False

			

			elif keys[pygame.K_d] and pn.characters.player.x < 1500 - pn.characters.player.width - pn.characters.player.vel:
				pn.characters.player.x += pn.characters.player.vel
				pn.characters.player.orientation = 0
				pn.characters.player.right = True
				pn.characters.player.left = False

			elif keys[pygame.K_w] and pn.characters.player.y > pn.characters.player.vel:
				pn.characters.player.y -= pn.characters.player.vel
				pn.characters.player.up = True
				pn.characters.player.down = False
				pn.characters.player.right = False
				pn.characters.player.left = False

			elif keys[pygame.K_s] and pn.characters.player.y < 900 - pn.characters.player.height - pn.characters.player.vel:
				pn.characters.player.y += pn.characters.player.vel
				pn.characters.player.orientation = 0
				pn.characters.player.up = False
				pn.characters.player.down = True

			else:		
				pn.characters.player.walk_count = 0

			if keys[pygame.K_SPACE]:
				self.fire()

			if keys[pygame.K_r]:
				if len(cbullet.bullets) > 0:
					for i in range(len(cbullet.bullets)):
						cbullet.bullets.pop(0)
						
				klik.player_click.click = -1
				klik.player_click.reloaded = True
				timer.add_timestamp()

keys = Keys()