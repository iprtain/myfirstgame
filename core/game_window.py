import pygame
import os
import classes.instances.player_and_npc as pn
import classes.bullet_class
import imageload.imageload as img
import core.collision_check  as cc

class GameWindow():
	def __init__(self):
		#sets winodw resolution
		self.win = pygame.display.set_mode((1500,900))
		#crosshair class instance
		self.crosshair = img.crosshair()

	#MOVES some npc's and DRAWS all bullets
	def draw_instance_ai(self, ai):
		x = 0
		if ai == pn.characters.ai1:
			x = 400
			ai.move(x)
		elif ai == pn.characters.ai2:
			x = 600
			ai.move(x)
		else:
			ai.move()
		for ai.ebullet in ai.enemy_bullets:
			ai.ebullet.draw(self.win)
	#DRAWS all npc's
	def draw_instance_e(self, e):
		e.draw(self.win)

	#everything that is actualy gonna be drawn during main game stage is set here
	#if i add more levels there might be more such functions and i'll change name
	def drawgamewindow(self):                                   
		self.win.blit(img.bg, (0, 0))
		pn.characters.player.draw(self.win)

		self.crosshair.draw(self.win)
		
		for bullet in classes.bullet_class.bullets:
			bullet.draw(self.win)

		for enemy in cc.collision.enemies:
			self.draw_instance_e(enemy)
		for role in cc.collision.roles:
			self.draw_instance_ai(role)

		pygame.display.update()

gamewindow = GameWindow()
