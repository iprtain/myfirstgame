import classes.instances.player_and_npc as p
import classes.bullet_class as cbullet

class Collision():
	def __init__(self):
		self.characters = p.characters
		self.roles = [self.characters.ai1,self.characters.ai2,self.characters.ai3,self.characters.ai4]
		self.enemies = [self.characters.e1, self.characters.e2, self.characters.e3, self.characters.e4]
		self.bullets = cbullet.bullets
	
	#tracking NPC bullets / compares NPC's bullets coordinates with player coordinates
	#calculates if there is collision between player and npc bullets
	#triggers self.alive inside enemy class into false
	def enemybullet_collision(self):
		for role in self.roles:
			for num in range(len(role.enemy_bullets)):
				if role.enemy_bullets[num].fired == True:
					if role.enemy_bullets[num].x >= self.characters.player.x and role.enemy_bullets[num].x <= self.characters.player.x + self.characters.player.width and \
					role.enemy_bullets[num].y >= self.characters.player.y and role.enemy_bullets[num].y <= self.characters.player.y + self.characters.player.height:
						role.enemy_bullets[num].fired = False
						if self.characters.player.alive == True:
							self.characters.player.hp -= 1
							if self.characters.player.hp == 0:
								self.characters.player.alive = False
	
	#same thing but this one calculates if there is any collision between players bullets and npc
	#trigers self.alive inside player class into false
	def playerbullet_collision(self):
		for bullet in self.bullets:
			for num in range(len(self.bullets)):
				if bullet.fired == True:
					for enemy in self.enemies:
						if bullet.x >= enemy.x and bullet.x <= enemy.x + enemy.width and \
						bullet.y >= enemy.y and bullet.y <= enemy.y + enemy.height:
							bullet.fired = False
							if enemy.alive:
								enemy.hp -= 1
								if enemy.hp == 0:
									enemy.alive = False
		

collision = Collision()
