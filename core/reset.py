import core.collision_check as cc
import classes.bullet_class
import core.time_functions as tf
import classes.instances.player_and_npc as pn
import classes.bullet_class as cbullet
import core.click as klik
import core.keys


#resets all the parameters to starting conditions
#used in new game option
def reset():
	classes.bullet_class.bullets = []
	tf.timestamp = 0
	tf.rof = 0
	pn.characters = pn.Positions()
	cc.collision.characters = pn. characters
	cc.collision.roles = [pn.characters.ai1, pn.characters.ai2, pn.characters.ai3, pn.characters.ai4]
	cc.collision.enemies =[pn.characters.e1, pn.characters.e2, pn.characters.e3, pn.characters.e4]                                       #characters ne znaci nista treba izmjeniti
	cc.collision.bullets = cbullet.bullets
	klik.player_click = klik.Click()
	core.keys.reloaded = True
	tf.timer = tf.Timer()

