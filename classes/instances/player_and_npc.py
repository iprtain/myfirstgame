from classes.player_class import *
import classes.ai_class as ai
import classes.ai_spec_class as spec

class Positions(): # class for player and npc's
	def __init__(self):

		self.player = Character(300, 800, 40, 75, psprites)


		self.e1 = Character(900, 200, 40, 75)
		self.e1.vel = 3

		self.e2 = Character(1200,500, 40, 75)
		self.e2.vel = 3

		self.e3 = Character(800,400,40,75)
		self.e3.vel = 3

		self.e4 = Character(200,200,40,75)
		self.e4.vel = 3

	#instances of AI classes		

		self.ai1 = ai.AI(self.e1)
		self.ai2 = ai.AI(self.e2)
		self.ai3 = spec.AIspec(self.e3)
		self.ai4 = spec.AIspec(self.e4)

characters = Positions()