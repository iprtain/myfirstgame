import pygame
import os
import classes.instances.player_and_npc
import math
import bullet.projectile as p

#this class is used by all instances of enemy npc created bullets
#it's little bit different from player's bullet class because it's not using mouse position, but instead, player's coordinates
class EnemyBullet():
	
	def __init__(self, x, y, width, height):                                      
		self.fired = False                                                        
		self.x = x + width//2
		self.y = y + height//2
		
		
		self.mx = classes.instances.player_and_npc.characters.player.x + classes.instances.player_and_npc.characters.player.width//2
		self.my = classes.instances.player_and_npc. characters.player.y + classes.instances.player_and_npc. characters.player.height//2

		self.speed = 10 

		self.radians = math.atan2(self.my-self.y, self.mx - self.x)
		#self.distance = math.hypot(mx -self.x, my - self.y)                                                           
		self.dx = math.cos(self.radians)*self.speed
		self.dy = math.sin(self.radians)*self.speed



	def draw(self,win):
		if self.fired and self.x < 1500 and self.y < 900:
			self.x += self.dx
			self.y += self.dy
			
			#print (bullets[0].x)
			win.blit(p.projectile[0], (self.x, self.y))
		else:
			self.fired = False

	def reload(self,enemy_bullets):                                               #empties the enemy_bullets list / bullets list used by enemies
		for bullet in enemy_bullets:
			enemy_bullets.pop(enemy_bullets.index(bullet))

