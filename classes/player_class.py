from animation.player_animation import *
import animation.enemy_animation as e
#main characther class inherited by AI class, used by player and npc's
class Character():                                             
	def __init__(self, x, y, width, height, sprites=e.esprites):
		#for correct load of animations
		self.sprites = sprites
		 #implemented for death
		self.hp = 3
		self.alive = True 
		# x and y are for coordinates                                    
		self.x = x                                                                           
		self.y = y
		self.width = width
		self.height = height
		#velocity
		self.vel = 5
		#used as switch, mostly to change direction of walking for npc  
		#if active, triggers animations in other parts of code                                                      
		self.switch = False                                   
		self.left = False                                     
		self.right = False
		self.up = False
		self.down = False
		self.walk_count = 0
		self.shoot = False
		self.shoot_count = 0
		self.death_count = 0
		self.stopdying = False
		if self.shoot != 0:                                         
			count = 0
			count += 1
			if count == 24:
				self.shoot = 0
		self.orientation = 0
		#for leaving characther animation facing the last direction he was moving                                      
		

	#function for displaying desired animations
	def draw(self, win):                                                 
#walking/standing animation
		if self.alive:

			pygame.draw.rect(win, (255,0,0), (self.x, self.y -20, 50, 10))
			pygame.draw.rect(win, (0,255,0), (self.x, self.y -20, 50 - (50 - (self.hp*17)), 10))
			#this counts frames, every 3 frames, displays different sprite, after 21 frames it resets on first sprite		
			if self.walk_count + 1 >= 21:
				self.walk_count = 0
			# e.g. every three frames image is replaced with new one
			elif self.left:                                                             
				win.blit(self.sprites.walk_left[self.walk_count//3], (self.x, self.y))
				self.walk_count += 1

			elif self.right:
				win.blit(self.sprites.walk_right[self.walk_count//3], (self.x, self.y))
				self.walk_count += 1

			elif self.up:
				win.blit(self.sprites.walk_up[self.walk_count//3], (self.x, self.y))
				self.walk_count += 1

			elif self.down:
				win.blit(self.sprites.walk_down[self.walk_count//3], (self.x, self.y))
				self.walk_count += 1

		
			else:
				if not self.shoot:
					if self.orientation == 0:
						win.blit(self.sprites.walk_right[0], (self.x, self.y))
					else:
						win.blit(self.sprites.walk_left[0], (self.x, self.y))

			
	#shooting animation
			if self.shoot == True:
				self.right = False
				self.left = False
				self.up = False
				self.down = False
				if self.shoot_count >= 24:
					self.shoot = False
					self.shoot_count = 0
				if self.orientation == 0:
					win.blit(self.sprites.shoot[self.shoot_count//3], (self.x, self.y))
					self.shoot_count += 1
				if self.orientation == 1:
					win.blit(self.sprites.shoot2[self.shoot_count//3], (self.x, self.y))
					self.shoot_count += 1
		else:
			if not self.stopdying:
				if self.death_count + 1 >= 12:
					self.stopdying = True
				win.blit(self.sprites.death[self.death_count//3], (self.x, self.y))
				self.death_count += 1
			else:
				win.blit(self.sprites.death[3], (self.x, self.y))