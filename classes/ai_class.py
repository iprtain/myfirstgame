import core.click as klik
from core.time_functions import *
from classes.enemy_bullet import *


#this class is inherited by some enemy() class instances
#2 of them exactly in this case
#it sets the behaviour of enemy npc
#npc might have been admittedly better name for this class
class AI():                                      
	def __init__(self,ai1):                      
		self.e1click = klik.Click()
		self.enemy_bullets = []
		self.timestamp = 10
		self.just_fired = False
		#used for setting the rate of fire
		self.shooting_time_stamp = 1
		#counts the actual shots made by enemy / it's not fully utilized because they don't get into situations of fireing 6 bullets 
		# and waiting to reload / but it's used to set how much bullets to fire in situation                  
		self.shotsfired = 0                           
		self.name = ai1                               
		self.start_used = False
		

	def addshotsfired(self):
		self.shotsfired += 1

	def reload(self):                                                   
		for bullet in self.enemy_bullets:
			self.enemy_bullets.pop(self.enemy_bullets.index(bullet))
		self.enemy_bullets.pop(0)
		self.shotsfired = 0
		self.e1click.click = -1



	def addtimestamp(self):
		self.timestamp = time.time()

	def aimeasure(eslf,sec, timestamp):
			elapsed = time.time() - timestamp
			if elapsed > sec:
				return True
	#this was used to measure time before shooting after npc stopped walking 
	def measure(self, sec):                                     
		elapsed_time = time.time() - self.timestamp
		if elapsed_time >= sec:
			return True
	#  for measuring time between shots, helps in adding rate of fire 
	def measure_last_shot(self,sec):                                       
		elapsed_time = time.time() - self.shooting_time_stamp
		if elapsed_time >= sec:
			return True

	#shooting function for AI, it was made in plan for first planned NPC moving lef-right, but i implemented it in others
	#ended up automatically calling it inside move function
	def aishoot(self):               	
		if self.measure(1) and self.shotsfired < 3 and not self.just_fired:
			self.e1click.add()
			self.name.shoot = True
			self.addshotsfired()
			self.enemy_bullets.append(EnemyBullet(self.name.x, self.name.y, self.name.width, self.name.height))
			self.enemy_bullets[self.e1click.click].fired = True
			self.shooting_time_stamp = time.time()
			if self.just_fired == False:
				self.just_fired = True
				
		
		if self.measure_last_shot(1) and self.shotsfired < 3 and self.just_fired:
			self.just_fired = True
			self.e1click.add()
			self.name.shoot = True
			self.addshotsfired()
			self.shooting_time_stamp = time.time()
			self.enemy_bullets.append(EnemyBullet(self.name.x, self.name.y, 20, 20))
			self.enemy_bullets[self.e1click.click].fired = True
	#sets the moving path for npc and shooting command at the end of the moving path		
	def move(self, destination):                                                       
		if self.name.alive:

			if self.name.x > destination and self.name.switch == False:

				self.name.x -= self.name.vel
				self.name.orientation = 1
				self.name.left = True
				self.name.right = False
				self.timestamp = time.time()
				
			else:
				self.name.left= False
				self.name.switch = True
				
				self.aishoot()
				
		
				if self.aimeasure(3.5, self.timestamp):
					if self.name.x < 900:
						
						self.name.x += self.name.vel
						self.name.orientation = 0
						self.name.left = False
						self.name.right = True
					else:
						self.name.right = False
						self.name.switch = False
						self.reload()
						self.aishoot()                                                                        
						#print('self.enemy_bullets length is ' + str(len(self.enemy_bullets)))