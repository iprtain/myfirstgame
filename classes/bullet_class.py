import pygame
import math
import os
import classes.enemy_bullet as e
import bullet.projectile


# this is bullet class
#instance is created when fireing key is used
#calculates direction between triggerman coordinates and mouse position, and sets speed
class Bullet(e.EnemyBullet):                                                       

	def __init__(self, x, y, width, height):                          
		self.fired = False                                            
		self.x = x + width//2
		self.y = y + height//2
		
		self.mpos = pygame.mouse.get_pos()
		self.mx = self.mpos[0]
		self.my = self.mpos[1]

		self.speed = 15

		self.radians = math.atan2(self.my-self.y, self.mx - self.x)
		#self.distance = math.hypot(mx -self.x, my - self.y)
		self.dx = math.cos(self.radians)*self.speed
		self.dy = math.sin(self.radians)*self.speed

		self.hitbox = (self.x, self.x + width, self.y, self.y - height)



	

		
bullets = []  