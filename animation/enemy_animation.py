import pygame
import os

def load(walk,name,i):
	walk.append(pygame.image.load(os.path.join('animation','enemysprites', name + str(i) + '.png')))
#most of things related with enemies use prefix E
#this class contains code for loading images used in enemy npc animation
class esprites():    
	walk_left = []
	for i in range (1,8):
		load(walk_left,'el', i)
	walk_right = []
	for i in walk_left:
		walk_right.append(pygame.transform.flip(i, True, False))

	walk_up = walk_left

	walk_down = walk_right

	shoot = []
	for i in range(1,6):
		load(shoot,'sd', i)
	load(shoot,'sd', 4)
	load(shoot,'sd', 3)
	load(shoot,'sd', 2)
	load(shoot,'sd', 1)

	shoot2 = []
	for i in shoot:
		shoot2.append(pygame.transform.flip(i, True, False))

	death = []
	for i in range(1,5):
		load(death,'death', i)

esprites = esprites()