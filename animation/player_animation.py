import pygame
import os


def load(walk,name,i):
	walk.append(pygame.image.load(os.path.join('animation','sprites', name + str(i) + '.png')))

#player characther animation
class psprites():
	walk_right = []
	for i in range (1,8):
		load(walk_right,'rd',i)
	walk_left = []
	for i in walk_right:
		#neat code za flipanje slika ako ih trebas zrcalno okrenuti
		walk_left.append(pygame.transform.flip(i, True, False))                                 

	walk_up = []
	for i in range(1,8):
		load(walk_up,'tl', i)

	walk_down = walk_right

	shoot = []
	for i in range(1,6):
		load(shoot,'sd', i)
	load(shoot,'sd', 4)
	load(shoot,'sd', 3)
	load(shoot,'sd', 2)
	load(shoot,'sd', 1)

	shoot2 = []
	for i in shoot:
		shoot2.append(pygame.transform.flip(i, True, False))

	death = []
	for i in range(1,5):
		load(death,'death', i)

psprites = psprites()
