import pygame
import os

intropic = pygame.image.load(os.path.join('intro','intro.jpg'))

gamewonpic = pygame.image.load(os.path.join('win_lose','victory.jpg'))

gamelostpic = pygame.image.load(os.path.join('win_lose','gamelost.jpg'))

bg = pygame.image.load(os.path.join('background','background9.jpg')) 
#background

crosshair_img = pygame.image.load(os.path.join('animation','crosshair', 'crosshair.png'))

class crosshair():
	def __init__(self):
		self.mpos = pygame.mouse.get_pos()
		self.x = self.mpos[0]
		self.y = self.mpos[1]

	def draw(self, win):
		self.mpos = pygame.mouse.get_pos()
		self.x = self.mpos[0] - 24
		self.y = self.mpos[1] - 24
		win.blit(crosshair_img, (self.x, self.y))
